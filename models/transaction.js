const mongoose = require('mongoose');
const { Schema} =  mongoose;
// const uniqueValidator = require("mongoose-unique-validator");

const transactionSchema = new Schema(
  {
  cart : {
    cartId : {
      type: Schema.Types.ObjectId, 
      ref: "Cart"
    }, 
  },
  subTotal : { //count dari cart
    type : Number,
    default : 0,
  },
  costDelivery : {
    type : Number,
    default : 0,
  },
  Total : {//totalprice of from cart + ongkir
    type : Number,
    default : 0,
  },
  status : { 
    type : String,
    required: true, //once payment done, the transaction turns out to success!
    enum: ['Success','On Process','Cancelled']
  },
  deliveries : {
    type : String,
    required : true,
    default : "",
  },
  product: { type: Schema.Types.ObjectId, ref: "Product", default: null },
  user: { type: Schema.Types.ObjectId, ref: "User", default: null },
}, { timestamps: true, versionKey: false })

const transaction = mongoose.model("Transaction", transactionSchema);

exports.Transaction = transaction;