const mongoose = require('mongoose');
const { Schema } = mongoose;
// const mongooseSocket = require('mongoose-socket.io');

let socketSchema = new Schema(
    {
        user: { 
            type: Schema.Types.ObjectId, 
            ref: "User", 
            default: null
        }
        // user: { 
        //     type: String,
        //     required: true
        // }
    },
    {
        message: {
            type: String,
            required: true
        }
    }
);

const message = mongoose.model("Message", socketSchema)
exports.Message = message;