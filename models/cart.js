const mongoose = require("mongoose");
const { Schema } = mongoose;

// module.exports = mongoose.model("Cart", CartSchema);

var cartSchema = new Schema({
    totalQty: { type: Number, default: 0 },
    totalPrice: { type: Number, default: 0 },
    totalWeight : { type: Number, default:0},
    items: [{product: {type: Schema.Types.ObjectId, ref: "Product"}, subTotal: {type: Number, default: 0},_id: false}, ] ,
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});
const cart = mongoose.model("Cart", cartSchema);

exports.Cart = cart;