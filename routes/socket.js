const express = require("express");
const router = express.Router();
const messageController = require("../controllers/socket");
const { Authentication } = require("../middlewares/auth");

router.get('/messages', messageController.GetAll)
router.post('/create', Authentication, messageController.Create)
router.post('/edit', messageController.Edit)
router.get('/find', messageController.Search)
router.get('/find/user', messageController.GetAllMessagesbyUser)

module.exports = router;