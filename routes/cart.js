// const express = require("express");
// const router = express.Router();

// const cartController = require("../controllers/cart");
// const { Authentication } = require("../middlewares/auth");
// const {uploader} = require("../middlewares/multer");

// router.get("/", Authentication, cartController.AddCart);
// });
// router.post(
//     '/create',
//     Authentication,
//     uploader.single("product_image"),
//     productController.Create)
// router.put(
//     '/update/:id',
//     Authentication,
//     uploader.single("product_image"),
//     productController.Update)
// router.delete(
//     '/delete/:id',
//     Authentication,
//     productController.Delete)
// router.get('/find', productController.Search)
// router.get('/:id', productController.GetProductId)
// router.get(
//     "/user",
//     Authentication,
//     productController.GetProductbyUser)

// module.exports = router;


const express = require("express");
const router = express.Router();
const { Authentication } = require('../middlewares/auth');

const cartControllers = require("../controllers/cart");

router.post("/add/:product_id", Authentication, cartControllers.addToCart);
router.put("/update/:id/:product_id", Authentication, cartControllers.addToCart);
router.get("/list", Authentication,cartControllers.getCart)
router.delete("/empty/:id", cartControllers.emptyCart);
router.delete("/delete/:id/:product_id", cartControllers.deleteProductCart)

module.exports = router;