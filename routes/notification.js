const express = require("express");
const router = express.Router();

const notificationController = require('../controllers/notification');
const { Authentication } = require('../middlewares/auth');
const { IsAdmin } = require("../middlewares/auth");

//admin
router.get('/admin', IsAdmin, notificationController.GetNotifications)
router.get('/admin/:notification_id', IsAdmin, notificationController.adminGetNotificationsID)

//user
router.get('/user', Authentication, notificationController.GetYourNotifications)
router.get('/user/:notification_id', Authentication, notificationController.userGetNotificationsID)


module.exports = router;
