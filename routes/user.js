const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/user");
const notificationController = require("../controllers/notification")
const { Authentication } = require("../middlewares/auth");
const {uploader} = require("../middlewares/multer");

router.post("/register", userControllers.Register);
router.post("/login", userControllers.Login);
router.get("/id", Authentication, userControllers.GetUserId);
router.get("/", Authentication, userControllers.GetUser);
router.put("/edit/:id",uploader.single('profile_image'), Authentication, userControllers.Edit);
router.delete("/delete/:id", Authentication, userControllers.Delete);

//notification
router.post('/notification/post/:transaction_id', Authentication, notificationController.postNotification)

module.exports = router;