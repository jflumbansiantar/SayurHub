const { Message } = require('../models/socket');
const mongoose = require('mongoose');
// const http = require("http").Server(app);
const io = require("socket.io");

exports.GetAll = async (req, res, next) => {
   try {
       const getMessages = await Message.find();
       res.status(200).json({
        success: true, 
        msg: "Succesfully retrieve all the messages!",
        getMessages
       });
   } catch (err) {
       next(err)
   }
}

exports.Create = async (req, res, next) => {
    try {
        let socket = {};
        const {message} = req.body;
        const userId = req.userData._id

        //checking data input
        // if(user) socket.user = user;
        if(message) socket.message = message;
        if(userId) socket.user = userId;
        
        // let messages = await Message.create(socket);
        let messages = await Message.findOneAndUpdate({
            _id: mongoose.Types.ObjectId()
        },
        socket,
        {
            new: true,
            upsert: true,
            runValidators: true,
            setDefaultsOnInsert: true,
            populate: {
                path: "user"
            }
        });
        io.emit('messages', req.body)
        console.log(socket.message);

        res.status(201).json({
            success: true,
            msg: 'Message sent!',
            messages
        })
    } catch (err) {
        next(err)
    }
}

exports.GetAllMessagesbyUser = async (req, res, next) => {
    try {
        const getMessages = await Message.find({
            user: userId
        });
        console.log(getMessages);

        res.status(200).json({
         success: true, 
         msg: "Succesfully retrieve all the messages!",
         getMessages
        });
    } catch (err) {
        next(err)
    }
 }

exports.Edit = async (req, res, next) => {
    /**
     * Algoritma
     * 1. Only authorize user can use this function 
     * 2. Search the user message by messageId
     * 3. Edit the message
     * 4. Return the message
     */
    //Let's try it by using Update Product
    try {
        const { id } = req.params;
        let socket = {};
        const {user, message} = req.body;

        //checking data input
        if(user) socket.user = user;
        if(user) socket.message = message;
        
        let updateMessages = await Message.findByIdAndUpdate(
            id,
            {$set: updateMessages},
            {new: true}
        );
        io.emit('updateMessages', req.body)
        console.log(updateMessages);

        res.status(201).json({
            success: true,
            msg: 'Message sent!',
            updateMessages
        })
    } catch (err) {
        next(err)
    }
}

exports.Search = async (req, res, next) => {
    try {
        const { messages } = req.body;
        let found = await Search.find({
            messages: {
                $regex: '%' + messages + '%'
            }
        });
        console.log(found)

        res.status(200).json({
           success: true,
           msg: "Successfully retrieve the messages!",
           found
        })
    } catch (err) {
        next(err)
    }
}

