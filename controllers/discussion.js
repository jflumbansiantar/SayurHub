const { User } = require('../models/user');
const { Admin } = require('../models/admin');
const { Product } = require('../models/product');
const { Discussion } = require('../models/discussion');
// const { Reply } = require('../models/reply');
const mongoose = require('mongoose');

exports.CreateUser = async (req, res, next) => {
    try {
        let obj = {};
        const { write } = req.body;
        const userID = req.userData._id;
        const productID = req.params.product_id;

        //checking data input
        if (write) obj.write = write;
        if (userID) obj.user = userID;
        if (productID) obj.product = productID;

        // const foundUser = await Discussion.findOne({ user: userID });
        // const foundProduct = await Discussion.findOne({ product: productID })

        // if (!(foundUser && foundProduct)) {
        // let discussions = await Discussion.create(obj);
        let discussions = await Discussion.findOneAndUpdate({
            _id: mongoose.Types.ObjectId()
        },
            obj,
            {
                new: true,
                upsert: true,
                runValidators: true,
                setDefaultsOnInsert: true,
                populate: { path: "user" }
            })
        // await User.findByIdAndUpdate(userID, {
        //     $push: { write: write._id },
        // });
        // await Product.findByIdAndUpdate(productID, {
        //     $push: { write: write._id },
        // });

        res.status(201).json({
            success: true,
            msg: 'Discussion created!',
            discussions
        })
        console.log(discussions)
        // } else {
        //     res.status(409).json({
        //         success: false,
        //         msg: 'Ada yang salah di controllernya!'
        //     })
        // }

    } catch (err) {
        next(err)
    }
}

exports.GetDiscussionByProduct = async (req, res, next) => {

    const productID = req.params.product_id;
    // if (productID) obj.product = productID;

    try {
        const foundDiscussion = await Discussion.find({
            product: productID
        });
        if (foundDiscussion) {
            res.status(200).json({
                success: true,
                message: "Successfully retrieve the data!",
                data: foundDiscussion
            })

        } else {
            res.send(404).json({
                success: false,
                msg: "There is no discussion in this product yet!"
            })
        }
        console.log(foundDiscussion)
    } catch (err) {
        next(err);
    }
}

exports.DeleteDiscussion = async (req, res, next) => {
    try {
        const id = req.params.id;

        await Discussion.findByIdAndRemove(id, (err, doc, result) => {
            if (err) throw "Failed to delete";
            if (!doc) return res.status(400).json({
                success: false,
                err: "Data not found!"
            });
            res.status(200).json({
                success: true,
                message: "Successfully deleted data!",
                data: doc,
            });
        })
    }
    catch (err) {
        next(err);
    }
}

// exports.EditDiscussion = async (req, res, next) => {
//     try {
//         const { discussion_id } = req.params;
//         let obj = {};
//         const { write } = req.body;

//         if (!discussion_id) return next({ message: "Missing ID Params" });
//         if (write) obj.write = write;

//         const edit = await Discussion.findByIdAndUpdate(
//             discussion_id,
//             { $set: obj },
//             { new: true }
//         );
//         res.status(200).json({
//             success: true,
//             message: "Successfully updated data!",
//             data: edit,
//         });
//         console.log(discussion_id)
//     }
//     catch (err) {
//         next(err);
//     }
// }
exports.EditDiscussion = async (req, res, next) => {
    try {
        const { id } = req.params;
        // console.log(req.params)

        if (!id) return next({ message: "Missing ID Params" })

        const edit = await Discussion.findByIdAndUpdate(
            id,
            { $set: req.body },
            { new: true }
        );
        res.status(200).json({
            success: true,
            message: "Successfully updated data!",
            data: edit,
        });
    }
    catch (err) {
        next(err);
    }
}
