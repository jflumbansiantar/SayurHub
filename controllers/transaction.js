const { Transaction } = require("../models/transaction");
const { User } = require("../models/user");
const { Product } = require("../models/product");
const { Cart } = require("../models/cart");
// const email = require("../helpers/notifEmail");
const {Notification} = require("../models/notification")
const mongoose = require('mongoose');

exports.Create = async (req, res, next) => {
    try {
		let obj = {};
		const cartId = req.userData._id
		//const userId = req.userData._id;
        const { subTotal, costDelivery, Total, status, deliveries } = req.body;

		//checking data input
		if(subTotal) obj.subTotal = subTotal;
		if(costDelivery) obj.costDelivery = costDelivery;
        if(Total) obj.Total = subTotal + costDelivery;
		if(status) obj.status = status;
		if(deliveries) obj.deliveries = deliveries;
		// if (userId) obj.user = userId;
		if (cartId) obj.cart = cartId;
		//console.log(_id);

		let transaction = await Transaction.findOneAndUpdate({_id:mongoose.Types.ObjectId()}, obj, {
            new: true,
            upsert: true, // create the data if not exist
            runValidators: true,
            setDefaultsOnInsert: true, // set default value based on models
			populate: {path: ("cart")},
			//populate: {path: ["user","product","cart"]},
		})
		// email(transaction._id, '--notif');
		res.status(201).json({
            success: true,
            msg: 'Transaction created!',
            transaction
        })
    } catch (err) {
        next(err)
    }
}

exports.AllTransaction = async (req, res, next) => {
	try {
	  let transaction = await Transaction.find().populate({
		//   path: "User, Produk"
	  })
	  res.status(200).json({
		success: true,
		message: "There is all the transaction data!",
		data: transaction,
	  });
	} catch (err) {
	  next(err);
	}
  };
  
  exports.TransactionById = async (req, res, next) => {
	try {
	const  userId  = req.userData._id;
	  let transaction = await Transaction.findOne({user: userID})
	  res.status(200).json({
		data: transaction,
	  });
	} catch (err) {
	  next(err);
	}
  };

  exports.Edit = async (req, res, next) => {
	try {
	  const { id } = req.params;
	  if (!id) return next({ message: "Missing ID Params" });
  
	  const updatedData = await Transaction.findOneAndUpdate(
		id,
		{ $set: req.body },
		{ new: true }
	  );
	  
	  res.status(200).json({
		success: true,
		message: "Successfully update a transaction!",
		data: updatedData,
	  });
	} catch (err) {
	  next(err);
	}
  };
  
  exports.Delete = async (req, res, next) => {
	try {
	  const { id } = req.params;
  
	  if (!id) return next({ message: "Missing ID Params" });
  
	  await Transaction.findOneAndRemove(id, (error, doc, result) => {
		if (error) throw "Failed to delete";
		if (!doc)
		  return res.status(400).json({ success: false, err: "Transaction not found!" });
  
		res.status(200).json({
		  success: true,
		  message: "Successfully delete transaction data!",
		  data: doc,
		});
	  });
	} catch (err) {
	  next(err);
	}
  };