const { User } = require('../models/user');
const { Admin } = require('../models/admin');
const { Notification } = require('../models/notification');
const { Cart } = require('../models/cart');
const { Transaction } = require('../models/transaction');
// const { Pusher } = require('pusher');
const mongoose = require('mongoose');

exports.GetNotifications = async (req, res, next) => {
    try {
        const adminID = req.userData._id;
        const foundAdmin = await Admin.findOne({ admin: adminID._id });
        console.log(foundAdmin, "--admin");

        if (!foundAdmin) {
            next({ message: 'Missing adminID params' });
        }
        else {
            let post = await Notification.find()
            console.log(post, "--post admin")
            res.status(200).json({
                success: true,
                message: "Successfully retrieve notification datas, jackass",
                data: post
            })

        }

    } catch (err) {
        next(err)
    }
}

exports.GetYourNotifications = async (req, res, next) => {
    try {
        const userID = req.userData._id;
        const foundUser = await User.findOne({ user: userID._id });
        console.log(foundUser, "--user");

        if (foundUser) {
            let post = await Notification.find()
            console.log(post, "--post user")
            res.status(200).json({
                success: true,
                message: "Successfully retrieve notifications data, pretty",
                data: post
            })
        } else {
            next({ message: 'Missing userID params' })
        }
    } catch (err) {
        next(err)
    }
}

exports.adminGetNotificationsID = async (req, res, next) => {
    const adminID = req.userData._id;

    if (!adminID) return next({ message: 'Missing userID Params' });

    try {
        const { id } = req.params;
        let post = await Notification.findOne({ id });
        res.status(200).json({
            success: true,
            msg: 'Successfully retrieve notification, jackass',
            post
        })
    } catch (err) {
        next(err)
    }
}

exports.userGetNotificationsID = async (req, res, next) => {
    try {
        const userID = req.userData._id;

        if (!userID) { return next({ message: 'Missing userID Params' }) }
        else {
            const { id } = req.params;

            let post = await Notification.findOne({ id });
            res.status(200).json({
                success: true,
                msg: 'Successfully retrieve notification, handsome',
                post
            })
        }
    } catch (err) {
        next(err)
    }
}

exports.postNotification = async (req, res, next) => {
    try {
        let obj = {};
        const userID = req.userData._id;
        const transactionID = req.params.transaction_id;

        if (userID) obj.user = userID;
        if (transactionID) obj.transaction = transactionID;
        let foundTransaction = await Transaction.findOne({ status: 'Success' })

        if (foundTransaction) {

            let adminNotification = await Notification.findByIdAndUpdate(
                { _id: mongoose.Types.ObjectId() },
                obj,
                {
                    new: true,
                    upsert: true,
                    runValidators: true,
                    setDefaultsOnInsert: true,
                    populate: { path: 'User' }
                }
            )

            res.status(200).json({
                success: true,
                message: "Hey, you got a notification here. Check it out, you jackass!",
                data: adminNotification
            });
            console.log(adminNotification, '--adminNotification');
        } else {

        }
    } catch (err) {
        next(err)
        // console.log('Nothing here!', '--adminNotification dilewatkan');

    }
}