# SayurHub
# Team SayurHub-Backend - E-Commerce Apps

Create a E-Commerce Apps (SayurHub)

Status Code Response
```
200 - OK                      > Call API success
201 - CREATED                 > Post success
202 - ACCEPTED                > Response/Post success
400 - BAD REQUEST             > Error on client side
404 - NOT FOUND               > Req.bodyrequest endpoint not found
409 - CONFLICT                > User not fill the requirement
500 - INTERNAL SERVER ERROR   > Error on server side
```
URL : https://pacific-oasis-23064.herokuapp.com

Image format must be in jpg, jpeg, png, svg.

# RESTful endpoints

## GET ((URL))/ : 
Homepage
```json
Request Header : not needed
```
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
    "message": "This is home page thanks."
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/user : 
Get All Users
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  success: true,
	message: "Successfully retrieve the data!",
	data: "<user data>"

}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/user/register : 
Register User
```json
Request Header : not needed
```
```json
Request Body: {
  "full_name": "<user name>",
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (201 - Created){
  {
    "success": true,
    "message": "Successfully create a user!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0,
        "createdAt": "<user time create>"
    }
  } 
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/user/login :
Login User
```json
Request Header : {
 not needed
}
```
```json
Request Body: {
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (200 - OK){
  success: true,
	message: "Successfully logged in!",
	token: "<your token>"
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find User or Password"
}
```

## PUT ((URL))/user/edit/:user_id : 
Edit Users
```json

Request Header : {
  "access_token": "<your access token">
}
```
```json

Request Body: {
  "full_name": "<user name>", 
  "profile_image" : "<user image>",
  "email": "<user email>", 
  "description": "<user description>"
}
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully update a user!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "<user description>",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0,
        "updatedAt": "<user time update>"
    }
  } 
}
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## DELETE ((URL))/user/delete/:user_jd: 
Delete Users
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## GetUserId ((URL))/user/id : 
GetUser By Id token
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
     "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "5f8ad3f14534ab053414b586",
        "full_name": "julia2",
        "email": "julia123@gmail.com",
        "password": "$2b$10$nsKb5YKYsRiFaPZdNGY6SeXG8USCapztMDsoB4Px260MAsUj9uule",
        "createdAt": "2020-10-17T11:22:25.426Z",
        "updatedAt": "2020-10-17T11:22:25.426Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

=======================================================================================================


## POST ((URL))/admin/login :
Login User
```json
Request Header : {
 not needed
}
```
```json
Request Body: {
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (200 - OK){
  success: true,
	message: "Successfully logged in!",
	token: "<your token>"
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find admin"
}
```

## Get ((URL))admin/: 
Get admin data
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "data": {
        "_id": "5f9babf03a40f73378032c06",
        "full_name": "sayurhub",
        "email": "sayurhub@gmail.com",
        "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
        "createdAt": "2020-10-30T06:00:16.554Z",
        "updatedAt": "2020-10-30T06:00:16.554Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
``` 

## Get ((URL))admin/data: 
Get all admin data
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "_id": "5f9babf03a40f73378032c06",
            "full_name": "sayurhub",
            "email": "sayurhub@gmail.com",
            "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
            "createdAt": "2020-10-30T06:00:16.554Z",
            "updatedAt": "2020-10-30T06:00:16.554Z"
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## Put ((URL))admin/edit/:id_admin: 
Edit data admin
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: {
  "full_name", "email"
}
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully edit data!",
    "data": [
        {
            "_id": "5f9babf03a40f73378032c06",
            "full_name": "sayurhub",
            "email": "sayurhub@gmail.com",
            "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
            "createdAt": "2020-10-30T06:00:16.554Z",
            "updatedAt": "2020-10-30T06:00:16.554Z"
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## Get ((URL))admin/user/: 
Get all Users 
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "profile_image": "https://res.cloudinary.com/waindinifitri/image/upload/v1602921267/ptb2eckxsuqh5qee689g.jpg",
            "description": "Jual Buah dan Sayur Murah",
            "transactions": "On progress",
            "_id": "5f89de133ef5c40f1cd3f9d9",
            "full_name": "julia",
            "email": "julia@gmail.com",
            "password": "$2b$10$uLLLq7.GrV/3wrXcIvMhcuAq24uhoXAq2U.5Z2RKK.ofdygiSqROC",
            "__v": 0,
            "updatedAt": "2020-10-17T07:54:27.998Z"
        },
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
``` 

## DELETE ((URL))admin/user/delete/:user_jd: 
Delete Users
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))admin/product/create : 
Create a product
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: {
    "product_name": "<product name>",
    "description": "<product description>",
    "category": "<product category>",
    "discount": "<product discount>",
    "price": "<product price>",
    "actualPrice": 0,
    "stock": "<product stock>",
    "weight": "<product weight>",
    "nutrition":"<product nutrition>",
    "farmer_supllier": "<product farmer_supllier>",
    "product_image": "<product image>",
}
```
```json

Response: (200 - OK){
   "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "price": "<product price>",
            "actualPrice": "<product actualPrice>",
            "stock": "<product stock>",
            "weight": "<product weight>",
            "nutrition":"<product nutrition>",
            "farmer_supllier": "<product farmer_supllier>",
            "product_image": "<product image>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT ((URL))admin/product/update : 
Update products
```json
Request Header : {
    "token": "<your token>" 
}
```
```json
Request Body: {
  Request Body: {
    "product_name": "<product name>",
    "description": "<product description>",
    "category": "<product category>",
    "discount": "<product discount>",
    "price": "<product price>",
    "actualPrice": 0,
    "stock": "<product stock>",
    "weight": "<product weight>",
    "product_image": "<product image>",
    "nutrition": "<product nutrition>",
    "farmer_supllier": "<product farmer_supllier>"
}
}
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Product  updated!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "price": "<product price>",
            "stock": "<product stock>",
            "weight": "<product weight>",
            "nutrition":"<product nutrition>",
            "farmer_supllier": "<product farmer_supllier>",
            "product_image": "<product image>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        }
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE ((URL))admin/product/delete/:products_id : 
Delete products
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "nutrition":"<product nutrition>",
        "farmer_supllier": "<product farmer_supllier>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

=======================================================================================================

## GET ((URL))/products : 
Get all products
```json

Request Query Params : {
  "page" : "<your page>"
}
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
     "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "actualPrice": "<product actualPrice>",
            "price": "<product price>",
            "stock": "<product stock>",
            "product_image": "<product image>",
            "weight": "<product weight>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```


## GET ((URL))/products/:product_id:
Seacrh product by id
```json

Request Header : not needed
```
```json

Request Body: not needed
```
```json

Request Params: needed
```
```json

Response: (200 - OK){
   "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": {
        "discount": 0,
        "user": {
            "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
            "description": "Please fill your description ",
            "transactions": "On progress",
            "_id": "5f8ac46da59c4000172b9887",
            "full_name": "joko anwar",
            "email": "asd@asd.co",
            "password": "$2b$10$j9WmF47T/MKXMwsT8F6mDeY7wsJS94XxCChk8qOypJMJQGXjv7Rke",
            "createdAt": "2020-10-17T10:16:13.990Z",
            "updatedAt": "2020-10-17T10:16:13.990Z"
        },
        "_id": "5f956465eb16d90017356072",
        "__v": 0,
        "category": "Fruit",
        "createdAt": "2020-10-25T11:41:25.922Z",
        "description": "Buah yg ada di dengkul",
        "price": 5000,
        "product_image": "https://res.cloudinary.com/waindinifitri/image/upload/v1603626085/spmzghczzb60pgw84wkh.jpg",
        "product_name": "Coba buah beda token",
        "stock": 50,
        "updatedAt": "2020-10-25T11:41:25.922Z",
        "weight": 1
    }
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find User or Password"
}
```

## GET ((URL))/products/find : 
Search products by its name
```json

Request Header : not needed,
```
```json

Request Body: {
  "product_name": "<product name>"
}
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully retrieve all the products that have same name.",
    "data": {
       "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
  } 
}
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## GET ((URL))products/user : 
Delete products
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))products/filter/fruits : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))products/filter/diets : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))products/filter/vegetables : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
========================================================================================

## GET ((URL))/reviews : 
Get all reviews
```json
Request Header : not needed
```
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "5f8d2c45f09e1305ac338af4",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e91f5f258e40c1064cbc3",
            "rating": 8,
            "review": "good",
            "createdAt": "2020-10-20T07:29:57.816Z",
            "updatedAt": "2020-10-20T07:31:15.377Z",
            "__v": 0
        },
        {
            "product": "5f8e7db207a0cb1a20b6e20d",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e947eb1b4361aac1c8c29",
            "rating": 10,
            "review": "very good",
            "createdAt": "2020-10-20T07:40:46.100Z",
            "updatedAt": "2020-10-20T07:40:46.100Z",
            "__v": 0
        },
        {
            "product": "5f8e7ea907a0cb1a20b6e20e",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e94b5b1b4361aac1c8c2a",
            "rating": 9,
            "review": "very good",
            "createdAt": "2020-10-20T07:41:41.737Z",
            "updatedAt": "2020-10-20T07:41:41.737Z",
            "__v": 0
        },
        {
            "product": "5f8d2c45f09e1305ac338af4",
            "user": "5f8e9c73d0650211f49e1dc2",
            "_id": "5f8e9d5b0048781c54cfbba6",
            "rating": 9,
            "review": "very good",
            "createdAt": "2020-10-20T08:18:35.227Z",
            "updatedAt": "2020-10-20T08:18:35.227Z",
            "__v": 0
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/reviews/create/:product_id
Create a review
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: {
    "rating": "<rating>",
    "review": "<review>",
}
```
```json

Response: (200 - OK){
   "success": true,
    "message": "Successfully create a review!",
    "data": {
        "product": "5f8fd6bafe00881fecf66efe",
        "user": "5f8e9c73d0650211f49e1dc2",
        "_id": "5f8fdaaefe00881fecf66f01",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-21T06:52:30.291Z",
        "updatedAt": "2020-10-21T06:52:30.291Z",
        "__v": 0
    }
}
```
```json
Response: (409 - Conflict){
   "success": false,
    "message": "You have reviewed this product before!"
}
```

## PUT ((URL))/reviews/update/:review_id
Update review
```json
Request Header : {
    "token": "<your token>" 
}
Reques Param : needed
```
```json
Request Body: {
    "rating": "<rating>",
    "review": "<review>"
}
```
```json
Response: (201 - Created){
     "success": true,
    "message": "Successfully update a review!",
    "data": {
        "product": "5f8d2c45f09e1305ac338af4",
        "user": "5f8d8e5512114407f861462c",
        "_id": "5f8e91f5f258e40c1064cbc3",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-20T07:29:57.816Z",
        "updatedAt": "2020-10-21T07:04:05.414Z",
        "__v": 0
    }
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/reviews/product/:product_id
Get review by product_id
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
   "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "5f8e7db207a0cb1a20b6e20d",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e947eb1b4361aac1c8c29",
            "rating": 10,
            "review": "very good",
            "createdAt": "2020-10-20T07:40:46.100Z",
            "updatedAt": "2020-10-20T07:40:46.100Z",
            "__v": 0
        }
    ]
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## DELETE ((URL))reviews/delete/:review_id
Delete review
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
    "success": true,
    "message": "Successfully deleted data!",
    "data": {
        "product": "5f8d2c45f09e1305ac338af4",
        "user": "5f8d8e5512114407f861462c",
        "_id": "5f8e91f5f258e40c1064cbc3",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-20T07:29:57.816Z",
        "updatedAt": "2020-10-21T07:04:05.414Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/reviews/rating/:product_id
Get rating
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
    "average_rating": 9,
    "total_reviewer": 1,
    "Product": "5f8d2c45f09e1305ac338af4"
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
========================================================================================

## GET ((URL))/cart/list: 
Get list cart
```json
Request Header : 
{token}

```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "status": true,
    "cart": [
        {
            "totalQty": 1,
            "totalPrice": 26000,
            "totalWeight": 1,
            "items": [
                {
                    "id": "5f9be34c774f1f1300e8d336",
                    "name": "Rock Melon",
                    "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
                    "price": 26000,
                    "weight": 1,
                    "stock": 10,
                    "subtotal": 26000,
                    "quantity": 1
                }
            ],
            "_id": "5fa3a5db01335a29089e20bc",
            "user": "5f9bb89c9953b71ce8dd10ae",
            "__v": 0
        }
    ]
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/cart/add/:product_id: 
Get list cart
```json
Request Header : not needed
{token}
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "totalQty": 1,
    "totalPrice": 26000,
    "totalWeight": 1,
    "items": [
        {
            "id": "5f9be34c774f1f1300e8d336",
            "name": "Rock Melon",
            "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
            "price": 26000,
            "weight": 1,
            "stock": 10,
            "subtotal": 26000,
            "quantity": 1
        }
    ],
    "_id": "5fa3a5db01335a29089e20bc",
    "user": "5f9bb89c9953b71ce8dd10ae",
    "__v": 0
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/cart/empty/:cart_id:
Empty all product in Cart
```json
Request Header : not needed
{token}
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Product deleted!",
    "doc": {
        "totalQty": 1,
        "totalPrice": 26000,
        "totalWeight": 1,
        "items": [
            {
                "id": "5f9be34c774f1f1300e8d336",
                "name": "Rock Melon",
                "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
                "price": 26000,
                "weight": 1,
                "stock": 10,
                "subtotal": 26000,
                "quantity": 1
            }
        ],
        "_id": "5fa3a5db01335a29089e20bc",
        "user": "5f9bb89c9953b71ce8dd10ae",
        "__v": 0
    }
}

```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
==================================================================================


## GET ((URL))/discussion/:product_id :
Get all the discussions in one product
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/discussion/reply/:discussion_id :
Get all replies in a discussion.
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "reply": "<reply message/answering the discussion.>",
            "discussion": "<discussion id>",
            "admin": "<admin id>",
            "_id": "<reply id>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/discussion/create/:product_id :
Create a discussion in a product by a user.
```json
Request Header : {
  "token": "<user token>"
}
```
```json

Request Body: {
  "write": "<write your discussion here/what do you want to ask about the product>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Discussion created!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}

```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/discussion/reply/:discussion_id :
Create a reply in a discussion by an admin.
```json
Request Header : {
  "token": "<admin token>"
}
```
```json

Request Body: {
  "reply": "<reply the discussion>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Reply created!",
    "data": [
        {
            "reply": "<reply message/answering the discussion.>",
            "discussion": "<discussion id>",
            "admin": "<admin id>",
            "_id": "<reply id>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT ((URL))/discussion/edit/:id :
Edit the discussion in one product by the same user
```json
Request Header : {
  "token": "<user token>"
}
```
```json
Request Body: {
  "write": "<write your discussion here/what do you want to ask about the product>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully updated data!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT ((URL))/discussion/reply/edit/:id :
Edit a reply in a discussion by the same admin.
```json
Request Header : {
  "token": "<admin token>"
}
```
```json

Request Body: {
  "reply": "<reply the discussion>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully updated data!",
    "data": [
        {
            "reply": "<reply message/answering the discussion>",
            "discussion": "<discussion id>",
            "admin": "<admin id>",
            "_id": "<reply id>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE ((URL))/discussion/delete/:id :
Delete a discussion in a product by the same user
```json
Request Header : {
  "token": "<user token>"
}
```
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully delete the data!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE ((URL))/discussion/reply/delete/:id :
Delete a reply in a discussion by the same admin.
```json
Request Header : {
  "token": "<admin token>"
}
```
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully delete the data!",
    "data": [
        {
            "reply": "<reply message/answering the discussion.>",
            "discussion": "<discussion id>",
            "admin": "<admin id>",
            "_id": "<reply id>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```