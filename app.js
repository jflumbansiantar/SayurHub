const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config();

const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const http = require("http").Server(app);
const io = require("socket.io")(http);

//parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//cors
app.use(cors());

//server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server-side connected to port ${PORT}`))

//io connection
io.on('connection', () => {
    console.log('A user is connected')
})

//db config
const mongoURI = process.env.MONGO_URI
mongoose.Promise = global.Promise;
const optoins = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
};
mongoose.connect(mongoURI, optoins);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connection to mongoDB has established!"))

//routes
const userController = require("./routes/user")
const adminController = require("./routes/admin")
const productController = require("./routes/product");
const discussionController = require("./routes/discussion")
const cartController = require("./routes/cart")
const paymentController = require("./routes/paymentStripe");
const transactionController = require("./routes/transaction");
const notificationController = require("./routes/notification");
const deliveryController = require("./routes/delivery");
const errorHandler = require("./middlewares/errorHandler");

app.use("/api/v1/products", productController);
app.use("/api/v1/admin", adminController);
app.use("/api/v1/users", userController);
app.use("/api/v1/discussion", discussionController);
app.use("/api/v1/cart", cartController);
app.use("/api/v1/transaction", transactionController);
app.use("/api/v1/payment", paymentController);
app.use("/api/v1/notification", notificationController);
app.use("/api/v1/delivery", deliveryController);
app.use(errorHandler);
