module.exports = (err, req, res, next) => {
    if(err){
        if(err.status){
            res.status(err.status).json({
                status: "failed",
                msg: err.message
            });
        } else if(err.response && err.response.data){
            res.status(400).json({
                status: "failed",
                msg: err.response.data.error
            })
        } else {
            res.status(400).json({
                success: "failed",
                msg: err.message
            })
        }
    }
        
}